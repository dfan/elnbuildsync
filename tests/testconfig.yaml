configuration:
  source:
    scm: https://src.fedoraproject.org
    cache:
      url: https://src.fedoraproject.org/repo/pkgs
      cgi: https://src.fedoraproject.org/repo/pkgs/upload.cgi
      path: "%(name)s/%(filename)s/%(hashtype)s/%(hash)s/%(filename)s"
    profile: koji
    mbs: https://mbs.fedoraproject.org
  destination:
    scm: https://src.fedoraproject.org
    cache:
      url: https://src.fedoraproject.org/repo/pkgs
      cgi: https://src.fedoraproject.org/repo/pkgs/upload.cgi
      path: "%(name)s/%(filename)s/%(hashtype)s/%(hash)s/%(filename)s"
    profile: koji
    mbs: https://mbs.fedoraproject.org
  trigger:
    rpms: f40
    modules: f40-modular
  build:
    prefix: git+https://src.fedoraproject.org
    target: eln
    platform: platform:eln
    scratch: true
  git:
    author: DistroBaker
    email: osci-list@redhat.com
    message: |
      Merged update from upstream sources

      This is an automated DistroBaker update from upstream sources.
      If you do not know what this is about or would like to opt out,
      contact the OSCI team.
  control:
    build: true
    merge: false
    strict: true
    autopackagelist:
        view: [ eln, eln-extras ]
        content_resolver: https://tiny.distro.builders
    skip_tag:
      rpms:
        # This package defines the %{rhel} and %{eln} macros and we should not
        # ever tag the Fedora version into ELN
        - ^fedora-release$
        # Rust cannot rebuild from the Fedora version due to incompatibility
        # between the enabled build targets
        - ^rust$
        # If other packages build against the Fedora LLVM, they will fail to
        # run against the ELN LLVM due to missing symbols. Avoid allowing LLVM
        # to tag the Fedora build into the buildroot.
        - ^llvm[0-9.]*
        - ^clang[0-9.]*$
        # The selinux-policy package provides RPM macros that other packages
        # use to set their minimum required policy (such as smartmon). This
        # dependency includes the %dist tag, meaning we cannot tag the Fedora
        # Rawhide version in, since it may break dependent packages built
        # during the window between the tag and the rebuild.
        - ^selinux-policy$
        # OCAML has special behavior and fedora builds cannot be mixed with ELN builds
        - ^ocaml.*
        # redhat-rpm-config contains many fundamental macros and should not be
        # put into the ELN buildroot
        - ^redhat-rpm-config$
        # The GTK packages are built with different optional features in ELN and
        # as a result are not fully binary-compatible with Fedora
        - gtk3
        - gtk4
        # The Fedora builds include a kde (kwallet) subpackage which pulls in Qt5/KF5
        - ^subversion$

    exclude:
      rpms:
        # it takes too much infra resources to try kernel builds automatically
        - ^kernel$
        # In RHEL kernel-headers is a sub-package of kernel
        - ^kernel-headers$
        # In RHEL kernel-tools is a sub-package of kernel
        - ^kernel-tools$
        # crypto-policies is manually synced from CentOS Stream
        - ^crypto-policies$
        # In RHEL rubygems is a sub-package of ruby
        - ^rubygems$
        # In RHEL rubygem-json is a sub-package of ruby
        - ^rubygem-json$
        # In RHEL rubygem-minitest is a sub-package of ruby
        - ^rubygem-minitest$
        # In RHEL rubygem-power_assert is a sub-package of ruby
        - ^rubygem-power_assert$
        # In RHEL rubygem-rake is a sub-package of ruby
        - ^rubygem-rake$
        # In RHEL rubygem-rdoc is a sub-package of ruby
        - ^rubygem-rdoc$
        # In RHEL rubygem-test-unit is a sub-package of ruby
        - ^rubygem-test-unit$
        # DistroBuildSync doesn't have permissions for secureboot packages
        - ^grub2$
        - ^fwupd$
        - ^fwupd-efi$
        - ^pesign$
        # shim has its own building procedure
        - ^shim.*
        # texlive is manually maintained due to its complexity
        - texlive
        - texlive-base
        # In RHEL, Rust packages are built with vendored dependencies
        # These packages keep the vendoring changes in ELN branch
        - ^rust-rpm-sequoia$
        - ^rust-sevctl$
        - ^virtiofsd$
        # Both netcat and nmap-ncat provide /usr/bin/nc; RHEL uses the latter
        - ^netcat$
        # RHEL-specific FIPS patches lag rawhide, being maintained in ELN branch
        - ^golang$
        # In RHEL golist is bundled, changes being maintained in ELN branch
        - ^go-rpm-macros$
        # These are built once for the oldest supported OS version and used for
        # each OS version's openjdk package
        - ^java-.*-openjdk-portable$
        # RHEL build is client-only, being maintained in ELN branch
        - ^ceph$
        # Maintained in the ELN branch
        # https://issues.redhat.com/browse/CS-1661
        - ^qemu$
  defaults:
    rpms:
      source: "%(component)s.git#rawhide"
      destination: "%(component)s.git#rawhide"
    modules:
      source: "%(component)s.git#%(stream)s"
      destination: "%(component)s.git#%(stream)s-rhel-9.0.0-beta"
    cache:
      source: "%(component)s"
      destination: "%(component)s"
components:
  rpms:
    ipa:
      source: freeipa.git
    ipa-healthcheck:
      source: freeipa-healthcheck.git
    restore:
      source: dump.git
    rhel-system-roles:
      source: linux-system-roles.git
